#include <ctime>
#include <fstream>
#include <iostream>
#include <raspicam/raspicam.h>
#include <unistd.h>
#include <wiringPi.h>
#include <zbar.h>
#include <opencv2/opencv.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace zbar;
using namespace cv;

#define W 1920
#define H 1080

int main ( int argc,char **argv ) {
    raspicam::RaspiCam_Still Camera;
    wiringPiSetup();

    time_t t1, t2;
    //Open camera
    cout<<"Opening Camera..."<<endl;
    if ( !Camera.open()) {cerr<<"Error opening camera"<<endl;return -1;}

   Camera.setCaptureSize(W, H);
   //Camera.setWidth(1920);
   //Camera.setHeight(1080);
   Camera.setFormat(raspicam::RASPICAM_FORMAT_GRAY);
   cout << "Ustawione\t" << Camera.getImageBufferSize() << endl;

    ImageScanner scanner;
    char buf[50];
    time(&t1);
    unsigned char *data=new unsigned uchar[  Camera.getImageTypeSize(raspicam::RASPICAM_FORMAT_RGB) ];
    //cout  << " allocated "  <<  Camera.getImageTypeSize(raspicam::RASPICAM_FORMAT_RGB) << endl;
    Camera.setRotation(180);
    //unsigned char *data=new unsigned char[ 6220800 ];
    for(int i = 0; ; i++)
    {
        if(i == 50) i=0;
        //Camera.grab();
	Camera.grab_retrieve(data, 3*1920*1080);
    	//allocate memory
	//extract the image in rgb format
	printf("To alloc %ui", raspicam::RASPICAM_FORMAT_RGB);
	//Camera.retrieve (data, raspicam::RASPICAM_FORMAT_IGNORE);//get camera image
    	//for(int k =0; k < 3; k++)
	//	memcpy(data+(k*1920*1080), Camera.getImageBufferData()+(k*1920*1080), 1920*1080);
	sprintf(buf, "/home/pi/Videos/P%i.jpg", i);
	std::ofstream outFile(buf , std::ios::binary);
	outFile<<"P6\n"<<Camera.getWidth() <<" "<<Camera.getHeight() <<" 255\n";
    	outFile.write ((char*)data, Camera.getImageTypeSize(raspicam::RASPICAM_FORMAT_RGB));
	outFile.close();

     Mat frame = imread(buf, -1);
     Mat grey;
     cvtColor(frame,grey,CV_BGR2GRAY);

     imshow("sda", frame);
     char c = waitKey(1);
     if(c=='a')
     {
     	break;
     }

     int width = frame.cols;
     int height = frame.rows;
     cout << "Img size : " << width << 'x' << height << endl;
     // wrap image data
     time(&t1);
     Image image(width, height, "Y800", (uchar*)grey.data, width * height);
     // scan the image for barcodes
     scanner.scan(image);
     time(&t2);
     cout << "B " << ctime(&t1) << "\b\b" << ctime(&t2) << endl;
     // extract results
     for(Image::SymbolIterator symbol = image.symbol_begin(); symbol != image.symbol_end(); ++symbol) {
         //vector<Point> vp;
     // do something useful with results
     cout << "decoded " << symbol->get_type_name() << " symbol \"" << symbol->get_data() << '"' <<" "<< endl;
     /*
     int n = symbol->get_location_size();
       for(int i=0;i<n;i++){
         vp.push_back(Point(symbol->get_location_x(i),symbol->get_location_y(i)));
       }
       RotatedRect r = minAreaRect(vp);
       Point2f pts[4];
       r.points(pts);
       for(int i=0;i<4;i++){
         line(frame,pts[i],pts[(i+1)%4],Scalar(255,0,0),3);
       }
    */
       //cout<<"Angle: "<<r.angle<<endl;
     }
     }
     /*

	ImageScanner scanner;
	Mat a = imread(buf);Mat gr;
	cvtColor(a, gr, CV_BGR2GRAY);
        int w = a.cols;
	int h = a.rows;
	//imshow("asdas", gr);
	//char c = waitKey(1);
	//if(c== 'a')
	//	exit(9);

	Image image = (w, h, "Y800", (uchar*)gr.data, w * h);
	scanner.scan(image);
	cout << "P" << i << endl;
	for(Image::SymbolIterator symbol = image.symbol_begin(); symbol != image.symbol_end(); ++symbol)
	{
		cout << symbol->get_data() << endl;
	}
	cout << "end\n";

	digitalWrite(1,1);
    	digitalWrite(0,0);
    	digitalWrite(7,1);
	if(i %2 == 0)
	{
    	  digitalWrite(1,1);
    	  digitalWrite(0,0);
    	  digitalWrite(7,0);
	}

    }//capture
    */
    time(&t2);
    cout << "B " << ctime(&t1) << " E " << ctime(&t2) << endl;
    
    //free resrources
    delete data;
    
    return 0;
}
