#include <ctime>
#include <fstream>
#include <iostream>
#include <raspicam/raspicam.h>
#include <unistd.h>
#include <wiringPi.h>
#include <zbar.h>
#include <opencv2/opencv.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <pthread.h>
using namespace std;
using namespace zbar;
using namespace cv;

typedef unsigned char uchar;

#define MAXRES_W 2592
#define MAXRES_H 1944
#define _SAVEFILE
//#define _DISPLAY
int W = 1600;
int H = 900;

enum Cams 
{
  CamA = 0,
  CamB = 1,
  CamC = 2,
  CamD = 3
};
int camPinout[4][3][2] = {
  // Reprezentacja konfiguracji pinów dla wyboru kamery
  // konwencja {pinID, HIGH/LOW}
  { {7,1}, {1,1}, {0,0} }, // CamA
  { {7,0}, {1,1}, {0,0} }, // CamB
  { {7,0}, {13,1}, {6,0} }, // CamC
  { {7,1}, {14,1}, {10,0} } // CamD
};

int connectedCams = 1;
int currentUsingCam = 0;
int thres = 100;
int SIZE;
Mat view360(H, W, CV_8UC1);
pthread_mutex_t imgMemCpy = PTHREAD_MUTEX_INITIALIZER;


void* scan_thread(void *ptr);
void updatePinConfiguration();
void select_next_cam();
void select_prev_cam();

int main ( int argc,char **argv ) {
  Mat tres;
  raspicam::RaspiCam Camera;
  time_t t1, t2;
  int threadID = 0;
  ImageScanner scanner;
  char buf[50];
  
  //wiringInit()
  wiringPiSetup();
  pinMode(7, OUTPUT);
  pinMode(1, OUTPUT);
  pinMode(0, OUTPUT);
  //pinMode(6, OUTPUT);
  //pinMode(10, OUTPUT);
  //pinMode(14, OUTPUT);
  //pinMode(13, OUTPUT);

  if(argc > 1)
  {
    connectedCams = atoi(argv[1]);
  }
  if(argc >= 3)
  {
    thres = atoi(argv[2]);
  }
  if(argc >= 5)
  {
    H=atoi(argv[4]);
    W=atoi(argv[3]);
  }

  	//mv to camInit()
  Camera.setFormat(raspicam::RASPICAM_FORMAT_GRAY);
  Camera.setCaptureSize(W, H);
  Camera.setRotation(180);
  SIZE = connectedCams * H * W;
  cout << "SIZE = " << SIZE << endl;
  cout << "Image size " << Camera.getImageTypeSize(raspicam::RASPICAM_FORMAT_GRAY) << " " << \
       Camera.getWidth() <<" "<<Camera.getHeight() << endl; 

  pthread_t** th = (pthread_t**)malloc(sizeof(pthread_t*)*3);
  for(int i=0; i<3;i++)
  {
    th[i] = (pthread_t*)malloc(sizeof(pthread_t));
  }
     
  updatePinConfiguration();
    //Open camera
  if ( !Camera.open())
  {
    cerr<<"Error opening camera"<<endl;
    return -1;
  }
  //cout << "Image size " << Camera.getImageTypeSize(raspicam::RASPICAM_FORMAT_GRAY) << endl; 
  //unsigned char *data=new unsigned uchar[Camera.getImageTypeSize(raspicam::RASPICAM_FORMAT_GRAY)];
  unsigned char *data=new unsigned uchar[H*W];
  for(int i = 0; ; i++)
  {
    if(i == 30) i=0;
    time(&t1);
    //cout << "[Taking snap] " << ctime(&t1); 
       
    pthread_mutex_lock(&imgMemCpy);
	  Camera.grab();
	  Camera.retrieve (data, raspicam::RASPICAM_FORMAT_IGNORE);//get camera image
    pthread_mutex_unlock(&imgMemCpy);
       
    time(&t2);
    
    //cout << "[Snap ready ] " << ctime(&t2);
       
    Mat frame(H, W, CV_8UC1, data);
    
    threshold(frame, tres, thres, 255, THRESH_BINARY);
    
    //adaptiveThreshold(frame, tres, 255, ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, atoi(argv[5]), 5);
    //threshold(frame, tres, thres, 255, ADAPTIVE_THRESH_MEAN_C);
    
    if(currentUsingCam == 0)
    { 
      view360.release();
      view360.create(frame.size(), frame.type());
      tres.copyTo(view360);
      //frame.copyTo(view360);
    }
    else
    {
      hconcat(view360, tres, view360);
      //hconcat(view360, frame, view360);
    }

    int width = tres.cols;
    int height = tres.rows;
#ifdef _SAVEFILE
    
    sprintf(buf, "/home/pi/Videos/P%i.jpg", i);
   
    if(currentUsingCam == connectedCams-1)
	  {
	    cout << "Saving " << endl;
      sprintf(buf, "/home/pi/Videos/P3.jpg");
	    imwrite(buf, view360);
    }
    
#endif
#ifdef _DISPLAY
    if( currentUsingCam == 0 ){
      //namedWindow("", WINDOW_NORMAL);		
      //imshow("sda", frame);
      imshow("sda", tres);
      char c = waitKey(1);
      if(c=='a')
      {
        break;
      }
    }
#endif
     //pthread_create(th[threadID], NULL, scan_thread, tres.data);
    threadID++;
    if(currentUsingCam == connectedCams-1)
    {
      for(int k=0;k<3;k++)
      {
        pthread_join(*th[k], NULL);
      }
       
      time(&t1);
      cout << "concatenated " << ctime(&t1);
      pthread_create(th[0], NULL, scan_thread, view360.data);
      pthread_join(*th[0], NULL);
      time(&t2);
      cout << "end " << ctime(&t2);
    }
    select_next_cam();
  }
  Camera.release();
  delete[] data;
  return 0;
}

void* scan_thread(void *ptr)
{
       time_t t1,t2;
       time(&t1);
       ImageScanner scanner;
       unsigned char *t = new uchar[SIZE];
       pthread_mutex_lock(&imgMemCpy);
       memcpy(t, (uchar*)ptr, SIZE);
       pthread_mutex_unlock(&imgMemCpy);
       Image image(connectedCams*W, H, "GREY", (uchar*)t, connectedCams*W*H);
       scanner.scan(image);
       for(Image::SymbolIterator symbol = image.symbol_begin();
		         symbol != image.symbol_end(); ++symbol)
       {
         cout << "decoded " << symbol->get_type_name() \
	      << " symbol \"" << symbol->get_data() << '"' \
	      <<" "<< endl;
       }
       time(&t2);
       //cout << pthread_self()  << ctime(&t1)  <<" - " << ctime(&t2);
       delete[] t;
       pthread_exit(0);
       
}

void updatePinConfiguration()
{
  for(int i=0; i<3; i++)
  {
    digitalWrite(camPinout[currentUsingCam][i][0], camPinout[currentUsingCam][i][1]);
  }
  usleep(500);
}

void select_next_cam()
{
  currentUsingCam = (currentUsingCam + 1) % connectedCams;
  updatePinConfiguration();
}

void select_prev_cam(){
  //ToDo
}
