#include <ctime>
#include <fstream>
#include <iostream>
#include <raspicam/raspicam.h>
#include <unistd.h>
#include <wiringPi.h>
#include <zbar.h>
#include <opencv2/opencv.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <pthread.h>
using namespace std;
using namespace zbar;
using namespace cv;

typedef unsigned char uchar;

#define MAXRES_W 2592
#define MAXRES_H 1944
#define _SAVEFILE
#define W 1920
#define H 1080

enum Cams
{
  CamA = 0,
  CamB = 1,
  CamC = 2,
  CamD = 3
};
int camPinout[4][3][2] = {
  // Reprezentacja konfiguracji pinów dla wyboru kamery
  // konwencja {pinID, HIGH/LOW}
  { {7,1}, {1,1}, {0,0} }, // CamA
  { {7,0}, {1,1}, {0,0} }, // CamB
  { {7,0}, {13,1}, {6,0} }, // CamC
  { {7,1}, {14,1}, {10,0} } // CamD
};

int connectedCams = 1;
int currentUsingCam = 0;
int thres = 100;
int SIZE = 3*W*H;
Mat view360(H, W, CV_8UC1);
pthread_mutex_t imgMemCpy = PTHREAD_MUTEX_INITIALIZER;
int MAX_IMAGE_NUMBER = 30;


void* scan_thread(void *ptr);
void updatePinConfiguration();
void select_next_cam();
void select_prev_cam();
void gauss(Mat, int , int , int);

int main ( int argc,char **argv ) {

    Mat tres;
    raspicam::RaspiCam Camera;
    time_t t1, t2;
    int threadID = 0;
    ImageScanner scanner;
    char buf[50];
    unsigned char *data=new unsigned uchar[Camera.getImageTypeSize(raspicam::RASPICAM_FORMAT_RGB)];


	//mv to camInit()
    Camera.setRotation(180);
    Camera.setCaptureSize(W, H);
    Camera.setFormat(raspicam::RASPICAM_FORMAT_GRAY);

    	// wiringInit()
    wiringPiSetup();

    if(argc > 1)
    {
      connectedCams = atoi(argv[1]);
    }
    if(argc == 3)
    {
      thres = atoi(argv[2]);
    }

    pthread_t** th = (pthread_t**)malloc(sizeof(pthread_t*)*thres);
    for(int i=0; i<thres;i++)
    {
       th[i] = (pthread_t*)malloc(sizeof(pthread_t));
    }

    //Open camera
    cout<<"Opening Camera..."<<endl;
    if ( !Camera.open()) {cerr<<"Error opening camera"<<endl;return -1;}

    for(int i = 0; ; i++)
    {
       if(i == MAX_IMAGE_NUMBER) i=0;
       time(&t1);
       //cout << "[Taking snap] " << ctime(&t1);

       pthread_mutex_lock(&imgMemCpy);
	Camera.grab();
	Camera.retrieve (data, raspicam::RASPICAM_FORMAT_IGNORE);//get camera image
       pthread_mutex_unlock(&imgMemCpy);

       time(&t2);
       //cout << "[Snap ready ] " << ctime(&t2);

       Mat frame(H, W, CV_8UC1, data);
       threshold( frame, tres, atoi(argv[1]), 255, THRESH_BINARY);

       if(threadID == 0)
       {
	 view360.release();
         view360.create(frame.size(), frame.type());
	 tres.copyTo(view360);
	 //cout << "mat size" << view360.size().width  <<endl;
       }
       else
       {
         hconcat(view360, tres, view360);
       }

       int width = frame.cols;
       int height = frame.rows;
#ifdef _SAVEFILE
	sprintf(buf, "/home/pi/Videos/P%i.jpg", i);
	if(threadID == 2) imwrite(buf, view360);
#endif
#ifdef _DISPLAY
       imshow("sda", frame);
       char c = waitKey(10);
       if(c=='a')
       {
          break;
       }
#endif
     //pthread_create(th[threadID], NULL, scan_thread, tres.data);
     threadID++;
     if(threadID == thres)
     {
       for(int k=0;k<3;k++)
       {
         pthread_join(*th[k], NULL);
       }
       threadID = 0;
       time(&t1);
       cout << "concatenated " << ctime(&t1);
       imshow("vision", frame);
       char c = waitKey(10);
       if(c=='a')
       {
          break;
       }
       pthread_create(th[0], NULL, scan_thread, view360.data);
       pthread_join(*th[0], NULL);
       time(&t2);
       cout << "end " << ctime(&t2);

     }
    }
    Camera.release();
    delete[] data;
    return 0;
}




void* scan_thread(void *ptr)
{
       time_t t1,t2;
       time(&t1);
       ImageScanner scanner;
       unsigned char *t = new uchar[SIZE];
       pthread_mutex_lock(&imgMemCpy);
       memcpy(t, (uchar*)ptr, SIZE);
       pthread_mutex_unlock(&imgMemCpy);
       Image image(3*W, H, "GREY", (uchar*)t, 3*W*H);
       scanner.scan(image);
       for(Image::SymbolIterator symbol = image.symbol_begin();
		         symbol != image.symbol_end(); ++symbol)
       {
         cout << "decoded " << symbol->get_type_name() \
	      << " symbol \"" << symbol->get_data() << '"' \
	      <<" "<< endl;
       }
       time(&t2);
       //cout << pthread_self()  << ctime(&t1)  <<" - " << ctime(&t2);
       delete[] t;
       pthread_exit(0);

}

void updatePinConfiguration()
{
  for(int i=0; i<3; i++)
  {
    digitalWrite(camPinout[currentUsingCam][i][0], camPinout[currentUsingCam][i][1]);
  }
  usleep(250);
}

void select_next_cam()
{
  currentUsingCam = (currentUsingCam + 1) % connectedCams;
  updatePinConfiguration();
}

void select_prev_cam(){

  if(currentUsingCam == 0) currentUsingCam = connectedCams -1;
  else currentUsingCam = currentUsingCam - 1

 updatePinConfiguration();
}


void gauss(Mat image, int size_, int x, int y){

    Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(size_, size_));
    Mat closed = new Mat();
    Imgproc.morphologyEx(image, closed, Imgproc.MORPH_CLOSE, kernel);

    image.convertTo(image, CvType.CV_32F);
    Core.divide(image, closed, image, 1, CvType.CV_32F);
    Core.normalize(image, image, 0, 255, Core.NORM_MINMAX);
    image.convertTo(image, CvType.CV_8UC1);

    for(int i=0;i<x;i++){
        for(int j=0;j<x;j++){
            Mat block = image.rowRange(y*i, y*(i+1)).colRange(y * i, y * (j+1));
            Imgproc.threshold(block, block, -1, 255, Imgproc.THRESH_BINARY_INV + Imgproc.THRESH_OTSU);
        }
    }


}
