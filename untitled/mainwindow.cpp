#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPicture>
#include <QImage>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QtGlobal>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QGraphicsScene* s = new QGraphicsScene();
    QImage p("/home/pi/Videos/P5.jpg");
    QGraphicsPixmapItem* pi = new QGraphicsPixmapItem(QPixmap::fromImage(p));
    s->addItem(pi);
    ui->view->setScene(s);
}

void MainWindow::rle()
{
    QImage p("/home/pi/Videos/P5.jpg");
    QGraphicsPixmapItem* pi = new QGraphicsPixmapItem(QPixmap::fromImage(p));
    ui->view->scene()->removeItem(ui->view->scene()->items().at(0));
    ui->view->scene()->addItem(pi);
    delete pi;
}

MainWindow::~MainWindow()
{
    delete ui;
}
