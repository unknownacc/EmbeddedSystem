#include "Updatethread.h"
#include <QLabel>
#include <QString>
#include <QPixmap>
#include <iostream>
#include <QImageReader>
#include <QImage>
#include <raspicam/raspicam.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/video.hpp>
#include <zbar.h>

using namespace cv;
using namespace zbar;
using std::string;

const int H = 1080;
const int W = 1920;
const int C = 1;
const int SIZE = H * W * C; 

updateThread::updateThread(QObject *parent) : QObject(parent)
{
}


void updateThread::process(){
    raspicam::RaspiCam Camera;
    Camera.setFormat(raspicam::RASPICAM_FORMAT_GRAY);
    Camera.setCaptureSize(W, H);
    Camera.setRotation(180);
    Mat thresholdFrame;
    
    if(!Camera.open())
	    std::cout << "Error opening camera!\n";
    unsigned char *data=new unsigned uchar[W*H*C];
    ImageScanner scanner;
    /*
    VideoCapture vid(0);
    vid.set(CV_CAP_PROP_FRAME_WIDTH,1600);
    vid.set(CV_CAP_PROP_FRAME_HEIGHT,900);
    vid.set(CAP_PROP_FORMAT , CV_CAP_PROP_CONVERT_RGB);
    Mat a;
    */
    
    //vid.open();
    while(1==1){
        //QString pathToJPEG = "/home/unknown/Pictures/test/";
        //string fileName = pathToJPEG + (j++) + ".jpg";
        //QString fileName = QString(pathToJPEG) + QString("ten.jpg");
        //std::cout << "Ddziala\n";
        Camera.grab();
        Camera.retrieve (data, raspicam::RASPICAM_FORMAT_IGNORE);//get camera image

        Mat frame(H, W, CV_8UC1, data);
        threshold(frame, thresholdFrame, 30, 255, THRESH_BINARY);
        Image image(W, H, "GREY", (uchar*)thresholdFrame.data, W*H);

        scanner.scan(image);
       for(Image::SymbolIterator symbol = image.symbol_begin();
		         symbol != image.symbol_end(); ++symbol)
       {
         std::cout << "decoded " << symbol->get_type_name() \
	      << " symbol \"" << symbol->get_data() << '"' \
	      <<" "<< std::endl;
       }

        QImage newImage(thresholdFrame.data, W, H, QImage::Format_Indexed8);
        //vid >> a;   
        this->ql->setPixmap(QPixmap::fromImage(newImage));        

        //newImage = reader.read();
        
    }
}
        //QString fileName = "/home/pi/Videos/P3.jpg";
        //QImageReader reader(fileName);
        //std::cout << "Running";
        //reader.setAutoTransform(true);
        //24 = QImage::Format_Grayscale8
        //Mat b;
        //cv::cvtColor(a, b, CV_8UC3, 3);