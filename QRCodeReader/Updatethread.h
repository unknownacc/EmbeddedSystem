#ifndef UPDATETHREAD_H
#define UPDATETHREAD_H

#include <QObject>
#include <QLabel>
#include <QWidget>

class updateThread : public QObject
{
    Q_OBJECT
public:
    explicit updateThread(QObject *parent = 0);
    //updateThread(QObject *parent = 0, QLabel *ql=NULL);
    void setQLabel (QLabel* ql){
        this->ql = ql;
    }

signals:
    void finished();
    void error(QString error);
public slots:
    void process();
private:
    QLabel *ql;
};

#endif // UPDATETHREAD_H
