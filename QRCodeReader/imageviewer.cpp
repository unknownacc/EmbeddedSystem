#include <QtWidgets>
#include "imageviewer.h"
#include "ui_imageviewer.h"
#include <QMessageBox>
#include <QDir>
#include <QImageReader>
#include <QLabel>
#include <QSize>
#include <iostream>
#include "Updatethread.h"

//#include <raspicam/raspicam.h>

ImageViewer::ImageViewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ImageViewer),
    imageLabel(new QLabel),
    scrollArea(new QScrollArea),
    scaleFactor(1)
{
    ui->setupUi(this);
    scrollArea->setWidget(imageLabel);
    scrollArea->setVisible(true);
    ui->verticalLayout->addWidget(this->scrollArea);
    QSize newSize(1920, 1080);
    ui->verticalLayoutWidget->resize(newSize);
    scrollArea->resize(newSize);
    imageLabel->resize(newSize);

    QThread *thread = new QThread();
    updateThread *updatethread = new updateThread();
    updatethread->setQLabel(imageLabel);
    updatethread->moveToThread(thread);
    //QObject::connect(updatethread, SIGNAL(error(QString)), this, SLOT(errorString(QString)));
    QObject::connect(thread, SIGNAL(started()), updatethread, SLOT(process()));
    QObject::connect(updatethread, SIGNAL(finished()), thread, SLOT(quit()));
    QObject::connect(updatethread, SIGNAL(finished()), updatethread, SLOT(deleteLater()));
    QObject::connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();

    std::cout << "[New thread has been created!]\n";
   //loadFile("/home/unknown/Pictures/11_11_by_kyrie0201-d83pu64.jpg");

}

bool ImageViewer::loadFile(const QString &fileName)
{
    QImageReader reader(fileName);
    //reader.setAutoTransform(true);
    const QImage newImage = reader.read();
    if (newImage.isNull()) {
        QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                                 tr("Cannot load %1: %2")
                                 .arg(QDir::toNativeSeparators(fileName), reader.errorString()));
        return false;
    }
//! [2]
    std::cout << "Loading image\n";
    //imageLabel->setGeometry(0,0,800,800);
    //imageLabel->setPixmap(QPixmap::fromImage(newImage));
    setImage(newImage);
    //const QString message = tr("Opened \"%1\", %2x%3, Depth: %4").arg(QDir::toNativeSeparators(fileName)).arg(image.width()).arg(image.height()).arg(image.depth());
    //statusBar()->showMessage(message);
    return true;
}

void ImageViewer::setImage(const QImage &newImage)
{
    //image = newImage;
    this->resize(newImage.size());
    ui->verticalLayoutWidget->resize(newImage.size());
    scrollArea->resize(newImage.size());
    imageLabel->resize(newImage.size());
    imageLabel->setPixmap(QPixmap::fromImage(newImage));
    scaleFactor = 1.0;

    scrollArea->setVisible(true);
    std::cout << "imageLoaded\n";
    imageLabel->setVisible(true);
}

ImageViewer::~ImageViewer()
{
    delete imageLabel;
    delete scrollArea;
    delete ui;
}
