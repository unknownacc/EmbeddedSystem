#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QMainWindow>
#include <QLabel>
#include <QScrollArea>

namespace Ui {
    class ImageViewer;
}

class ImageViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit ImageViewer(QWidget *parent = 0);
    QLabel* getQLabel(){
        return this->imageLabel;
    }

    bool loadFile(const QString &fileName);
    void setImage(const QImage &newImage);
    ~ImageViewer();

private:
    Ui::ImageViewer *ui;

    QImage image;
    QLabel *imageLabel;
    QScrollArea *scrollArea;
    double scaleFactor;
};

#endif // IMAGEVIEWER_H
